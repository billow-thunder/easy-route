# Easy Route for Vue Router

[![Vue](https://forthebadge.com/images/badges/made-with-vue.svg)](https://vuejs.org)

A middleware system that ties into Vue Router. It allows you to define guards and protect routes.

## Installation

Install Easy Route with npm.

```shell
  npm i @billow/easy-route
```

## Usage

Import:

```javascript
import EasyRoute from '@billow/easy-route' 
```

Install:

Easy Route requires a [Vue Router](https://router.vuejs.org/) instance to be passed in the options.

Additionally you may pass an array of guards to the plugin, but this will be discussed futher down.

```javascript
import router from '@/router'

Vue.use(EasyRoute, {
  router
})
```

Installing the plugin makes Easy Route aware of navigation through the [beforeEach](https://router.vuejs.org/guide/advanced/navigation-guards.html) method on the Vue Router API. It will however not have any effect until guards are added to the mix.

## Routes

The `Route` method has been designed to simplify route definitions. This method will return a route object for Vue Router.


```javascript
import { Route } from '@billow/easy-route'

Route('/path', 'route-name', VueComponent, ['stacked', 'middlewares'])

```

**Route Paramaters**

| Paramater     | type          | default  |
| ------------- |---------------| ---------|
| path          | string        | required |
| name          | string        |   ''     |
| component     | Vue Component | required |
| middleware    | Array         | []       |

## RouteGroup

The `RouteGroup()` method gives you the ability to group routes with `common middlewares` or `path prefixes` together.

```javascript
import { Route, RouteGroup } from '@billow/easy-route'

// Example components
import UserIndex from '@/components/user/Index'
import CreateUser from '@/components/user/Create'

RouteGroup({
    
    // Prefix all routes in group
    prefix: '/admin',
    
    // Apply guards to all routes in group
    middleware: [
        'auth',
        'admin'
    ],
    
    routes: [
        Route('/users', 'user-index', UserIndex),
        Route('/user', 'create-user', CreateUser, ['can-create-user']) // Additional guard on single route
    ]
    
})

```

**RouteGroup Options**

| Paramater     | type          | default  |
| ------------- |---------------| ---------|
| prefix        | String        | ''       |
| middleware    | Array         | []       |
| routes        | Array         | required |
| parent        | Route         | null     |


## RouteGroup with children

It is quite common to require routes with nested children. In these scenarios you may use the `parent` option of a RouteGroup. parent should be a Route()
and should not include a name. Once you have added the parent option, the routes array will 
become the children of the parent. 

**Note**

Easy Route will strip names & prefixes on parent routes. Child routes will inherit their prefix from the parent route.


```javascript
import { Route, RouteGroup } from '@billow/easy-route'

// Example components
import UserManager from '@/components/user/Manager'
import UserOverview from '@/components/user/Overview'
import EditUser from '@/components/user/EditUser'

RouteGroup({
    
    parent: Route('/users/:userId', '', UserManager),
    
    middleware: ['auth', 'admin'],
    
    routes: [
        Route('/', 'user-overview', UserOverview),
        Route('edit', 'edit-user', EditUser)
    ]
    
})

```

## Guards

At this point, Easy Route does not ship with any built in guards, each app is unique and needs its own set of guards.
Having said that creating custom guards for your application is very easy. 

Let's look at an example of what an auth guard might look like:

```javascript
// guards/auth.js

let guard = (context) => new Promise((resolve, reject) => {
    if (authenticated === true) {
        resolve()
    } else {
        reject({
          name: 'login',
          path: '/login' // optional, using a named route is better
        })
    }
})

export {
    name: 'auth',
    run: guard
}

```

In a nutshell guards allow you to protected your routes.

Your Guards must:

1. Return an object with a `name` key and `run` key. // { name: 'guard-name', run: Promise() }
2. `run` must return a promise
3. if `run` fails you must provide a fallback route. // reject({ name: '404' })

Your Guards will receive the current route context as the only parameter.

## Installing Guards

```javascript
// main.js
import auth from '@/guards/auth'
import EasyRoute from '@billow/easy-route' 

// Guard Example. 
// Please make sure you use your applications method of determining auth.
// The below is strictly an example of a guard.

Vue.use(EasyRoute, {
  router,
  guards: [auth]
})

```

## Putting it all together

```javascript
import Vue from 'vue'
import VueRouter from 'vue-router'
import { concat } from 'lodash'
import auth from '@/guards/auth'
import guest from '@/guards/guest'
import EasyRoute from '@billow/easy-route' 
import { Route, RouteGroup } from '@billow/easy-route'

Vue.use(VueRouter)

let guestRoutes = RouteGroup({
    
    middleware: ['guest'],
    
    routes: [
        Route('/', 'welcome', WelcomePage),
        Route('/login', 'login', LoginPage)
    ]
    
})

let appRoutes = RouteGroup({
    
    middleware: ['auth'],
    
    prefix: '/app',
    
    routes: [
        Route('/', 'dashboard', AppDashboard),
        Route('/users', 'user-index', UserIndex)
    ]
    
})

let router = new Router({
    routes: concact(
        guestRoutes,
        appRoutes
    )
})

Vue.use(EasyRoute, {
    router,
    guards: [
        auth,
        guest
    ]
})

new Vue({
    ...
    router
    ...
})

```



